/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personalData(){
		alert("Hi! Please enter your details!");
		let fullName = prompt("Enter your full name: ");
		let enterAge = prompt("Enter your age: ");
		let enterLocation = prompt("Enter your location: ");

		console.log("Hello, " + fullName);
		console.log("You are " + enterAge + " years old.");
		console.log("You live in " + enterLocation + " City");

		};

	personalData();
/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	 function displayFavoriteBands(){
	 	console.log("1. Rivermaya")
	 	console.log("2. Hale")
	 	console.log("3. True Faith")
	 	console.log("4. Eraserheads")
	 	console.log("5. Orange and Lemons")

	 };

	 displayFavoriteBands();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayFavoriteMovies(){
		console.log("1. AVENGERS: ENDGAME (2019)")
		console.log("Rotten Tomatoes Rating: 94%")
	 	console.log("2. Pearl (2022)")
	 	console.log("Rotten Tomatoes Rating: 90%")
	 	console.log("3. Hellraiser (2022)")
	 	console.log("Rotten Tomatoes Rating: 66%")
	 	console.log("4. MATRIARCH (2022)")
	 	console.log("Rotten Tomatoes Rating: 78%")
	 	console.log("5. Split (2016)")
	 	console.log("Rotten Tomatoes Rating: 78%")
	};

	displayFavoriteMovies();
/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	//let printFriends() = 

    console.log("---------");
	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();  

// console.log(friend1);
// console.log(friend2);
